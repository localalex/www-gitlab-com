---
layout: markdown_page
title: Courses
description: Learn about GitLab courses, including course listings and information for adding and uploading courses.
---

## On this page
{:.no_toc}

- TOC
{:toc}


## Deprecation

We are deprecating this page.
As noted in [our handbook guidlines](/handbook-usage/#guidelines) we want to organize per function and result, not per format.
All video's on this page should be embedded in the relevants part of the handbook.

## Adding a Course
{: #adding-a-course}

If you have anything to share (no matter the quality level) please add it to this page by:

1. Making sure all the content is publicly viewable. Upload video's to our YouTube channel. If there is a presentation in Google Sheets make sure anyone can view it. If there is written content either add it to the relevant part of the handbook or create a page like https://about.gitlab.com/handbook/people-operations/courses/sls-101 and assign the merge request to Abby Matthews who will happily review and merge it for you.
1. Give the code a unique identifier in the form of AAA111, first three letters are for the department, numbers are unique and first number specifies the difficulty level of the course. As a guideline, in many university settings, there are only two levels, i.e. 1xx for "introductory" courses, and 2xx for more advanced courses.
1. Add the course to the bottom of this page. If you made a course list on another page (like university or support) you can use just one link to link to the entire list. If the courses are not in one list please link to each individually.

Notes:

- The courses are part of our handbook or documentation and versioned with git so people can contribute via merge requests.
- All videos are publicly listed on Youtube under our account so they are easy to discover and accessible from many different platforms.
- Please use Youtube instead of Google Drive that only be viewed by team members. YouTube's functionality is superior (watch later, caching around the world, support by mobile telephone providers, speed control). But allowing users (potential customers) and applicants (potential team members) to watch and sent it to other people also greatly increases the impact of them. If needed use the 'Organization code names' doc to obfuscate customer names.

## Getting Access to Youtube
{: #getting-access-to-youtube}

1. If you don't have access to the Youtube channel you can find the details in 1password in the Team Vault.
1. Type in Youtube, you should see a secure note with the information on how to get added to the account.

## Uploading Courses to Youtube
{: #uploading-courses-to-youtube}

 - To upload the video, go to YouTube and click the up arrow at the top right corner, next to the GitLab profile picture.
 - Change the security level from "public" to either "unlisted" (only those with the link can view) or "private" (only people with access to the GitLab YouTube account can view), so that you can edit the video prior to it being live.
 - Under "Basic Info", change the title to follow this pattern: INF 201 - Using Terraform to manage the GitLab.com infrastructure
 - After the video is done uploading, click "Video Manager" in the bottom right corner.
 - Edit the video to start when the training actually starts:
        - Click Edit next to the video icon.
        - Click the Enhancements tab on the top menu bar.
        - Click Trim on the bottom right. Slide the left edge of the bar to a few moments before the training begins, and the right edge of the bar to a few moments after the training ends. Click **Done**.
Take a screenshot of the second slide of the presentation (if applicable) to make it as custom thumbnail for your video on YouTube. You can upload your custom thumbnail under the Info and Settings tab when you are editing a video.
After the video is finished being edited, change the security level back to "public".

## Training Channel

Once a course has been added to this page the link will be shared in the #training channel. The channel is also the place to add links for any other trainings that team members have been on (outside of GitLab) and would like to share and recommend to others.

## Language Courses

If you have any language courses you would like to recommend or links to websites please add them to this section.

 - [The 8 Best Interactive Websites for Adults to Learn English](https://www.fluentu.com/blog/english/best-websites-to-learn-english/)


### Individual Contributor (IC) courses

To add

### Manager (MGR) courses

To add


#### Compliance (COM) Courses

For some courses, there are legal requirements on the length and subject matter that must be provided to managers and individual contributors. In these cases, external training content has been purchased to meet these requirements. An example of this is:

- [COM 001 Common Ground: Prevention of Harassment, Sexual Harassment, and Abusive Conduct in the workplace (2 hrs)](https://learning.willinteractive.com/) This training will be assigned to all managers using [Will Interactive's content and LMS platform](https://willinteractive.com/). Details on how to use the platform can be found on the [learning and development page.](https://about.gitlab.com/handbook/people-operations/learning-and-development/index.html#common-ground-harassment-prevention-for-managers)


### Leadership (LDR) courses

- LDR 101 [One-on-Ones: Guidance at GitLab video](https://www.youtube.com/watch?v=KUxxjGJv1dQ&t=12s) and [slide deck](https://docs.google.com/presentation/d/1h6KdMQFFWco3pSoCD3WrgIaxL06w6NCtkMg4ZYmAqBA/edit#slide=id.g1e97e26035_2_30)
- LDR 102 [Underperformance](https://www.youtube.com/watch?v=nRJHvzXwXBU&list=PLFGfElNsQthYYlad7vtTUt3wXKQUTsZWz&index=3) and [slide deck](https://docs.google.com/presentation/d/1kVbFPzuVpjXhHNG2po8-j3013RUEYN0jK4n8ecbCU9I/edit#slide=id.g1fa059f00f_0_0)
- LDR 103 [Career Mapping](https://www.youtube.com/watch?v=YoZH5Hhygc4) and [slide deck](https://docs.google.com/presentation/d/1zqfXI5WNwr8XsvtBusP9zG_gFYDE-WJZPEU4A_EcM7U/edit#slide=id.g24bdf5a984_1_56)
- LDR 104 [Promotions](https://www.youtube.com/watch?v=TNPLiYePJZ8&t=492s) and [slide deck](https://docs.google.com/presentation/d/1QvWSTO2NCfz8XBMwnUBElvx-d7zGIi3WIEl8L9DR4tg/edit#slide=id.g1fa059f00f_0_0)
- LDR 105 [How to Give Performance Review Feedback](https://youtu.be/VK8cA8nYcoY) and [slide deck](https://docs.google.com/presentation/d/1obMvPNicsQBB8_vQC1OaQnsNPLzKkoxvzxFoQCPjzuU/edit#slide=id.g1fa059f00f_0_0)
- LDR 106 [Decision Making](https://youtu.be/7RbVQwU69H0) and [slide deck](https://docs.google.com/presentation/d/1ua-EShTeT1PD23Yw-5C2OTnqy8x58nR--b6QeRKeToI/edit#slide=id.g1fa059f00f_0_0)
- LDR 107 [Diversity & Inclusion Pt1](https://youtu.be/5bCmjiK8h5w) and [slide deck](https://docs.google.com/presentation/d/1lUACwyEGJAjhAB6HAcgXIP3CjTKz4nBzeKwoOsKsWGA/edit#slide=id.g25803b2e16_0_97)
- LDR 108 [Coaching](https://www.youtube.com/watch?v=uMs0Gzf5Gkw) and [slide deck](https://docs.google.com/presentation/d/1zGSfMSJeCjFQN7UPdVxV7Lg-u5gbCDfVMjLsSG0dmlw/edit#slide=id.g2a1e9c2bfd_0_178)
- LDR 109 [Reference Checks](https://www.youtube.com/watch?v=8pdf_rRihcE) and [slide deck](https://docs.google.com/presentation/d/18UevZe1tpngZiaejcWEwuITYvvi7wEBch7yc26eGHHM/edit#slide=id.g156008d958_0_18)
- LDR 110 [Performance Management Feedback](https://www.youtube.com/watch?v=D6Ko3_pVGjw)and [slide deck](https://docs.google.com/presentation/d/1DKPTU2eskWXs8b8NG_4mkiLvu5oLIbYgoLwJCLmous8/edit#slide=id.g29a70c6c35_0_68) 


### University (UNI) courses

- TODO Code all courses on [https://docs.gitlab.com/ee/university/](https://docs.gitlab.com/ee/university/) and add a single link from here (instead of listing all courses which would lead to duplication).

### Sales (SLS) courses

 - SLS001 [How to Conduct an Executive Meeting](https://www.youtube.com/watch?v=PSGMyoEFuMY&feature=youtu.be)

### Customer Success (CST) courses

**Office Hours**

- CST101 [GitLab I2P In-depth with Mark P.](https://youtu.be/fzlDQ2j-jv8)

**Explainer Videos**

- CST201 [GitHub.com vs. GitLab](https://www.youtube.com/watch?v=ZdmDJFPNQuI&index=1&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST202 [Hosting Gitlab in the Cloud](https://www.youtube.com/watch?v=1OLCDtUkw0Q&index=2&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST203 [Integrating Atlassian and GitLab](https://www.youtube.com/watch?v=o7pnh9tY5LY&index=3&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST204 [GitLab Maintenance and Support](https://www.youtube.com/watch?v=X8jsj59b4vk&index=4&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST205 [We already use JIRA, so shouldn't we go with BitBucket?](https://www.youtube.com/watch?v=-JRab22h9Dg&index=5&list=PLFGfElNsQthbFw3QxWjoTsDMoLmZx1SbP)
- CST206 [Traditional DevOps Dasiy Chain](https://www.youtube.com/watch?v=YHznYB275Mg)
- CST207 [Automate to Accelerate](https://www.youtube.com/watch?v=dvayJWwzfPY&t=465s)
- CST208 [Installing GitLab on GKE](https://www.youtube.com/watch?v=HLNNFS8b_aw&t=1s)
- CST209 [Connecting GitLab.com to your private cloud on GCP](https://www.youtube.com/watch?v=j6HRDquF0mQ)
- CST210 [Complete DevOps with GitLab](https://www.youtube.com/watch?v=68rGlAihKFw)

### Finance (FIN) courses

- TODO

### Build (BLD) courses

- BLD001 [GitLab Pivotal Cloud Foundry Tile](https://youtu.be/oo2p6WtHhG4)
- BLD002 [GitLab Terraform Module](https://youtu.be/JbbKq0UrDec)
- BLD003 [Kubernetes](https://youtu.be/Po8vUvoiMYU)
- BLD004 [Omnibus](https://youtu.be/m89NHLhTMj4)
- BLD005 [PG HA](https://youtu.be/2Uz2piFLp7k)
- BLD006 [GitLab QA project](https://youtu.be/Ym159ATYN_g)

### Engineering (DEV) courses

- [DEV 101 - Contributing to Golang projects](/courses/dev-101)

### Backend (BCK) courses

 - TODO

### Infrastructure (INF) courses

- [INF 201 - Using Terraform to manage the GitLab.com infrastructure](/courses/inf-201)
